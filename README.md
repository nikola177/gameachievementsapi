# Game Achievements API

## Description

This project presents design and implementation for an HTTP/JSON web service (API) for managing achievements for a
pre-existing set of games.

## Technologies

- Sring Boot
- Spring Data JPA
- SQL

## Data model

MySQL local host is intended for storing and managing data.

***Achievements***

- **Id** is a unique string that is generated by the system. I will use this unique ID
(for example "*265f74b8-9d44-4f7c-a029-4487cd1caf93*"​) to refer to the achievement in this API.
- **Display Name** is a short name of the achievement.
- **Description** is a concise description of achievement. Usually, this tells
  player how to earn the achievement. 
- **Icon** denotes the URL of the icon for this specific achievement.
- **Display Order** is the order in which the achievements are returned (for example, “1”).
- **Created** is a timestamp that denotes when the achievement was created.
- **Updated**​ is a timestamp that denotes when the achievement was last updated.
- Each achievement is associated with a specific ​**Game**.

***Games***

- **Id** is a unique string that is generated by the system. I will use this unique ID
(for example "*nigc1ghx-sw2a-2ikp-qbnm-2b5faghncmee7*"​) to refer to the game in this API.
- **Display Name** is a short name of the game (for example “Harry Potter”​).
- Each game has multiple **​Achievements​** associated with it.

## API Specification

Game Achievements API is used to configure achievements for a specific game by
exposing all achievement-related CRUD operations as API endpoints:

- **Create Achievement** ​allows adding a new achievement.  It takes all
of the relevant parameters in the request and persists this information in the
database.
- **Get All Game Achievements** ​returns all achievements for the supplied
game Id. Order of returned achievements is determined by the ​Display Order
attribute.
- **Get Achievement**​ returns a single achievement for the supplied achievement id.
- **Update Achievement** allows updating an existing achievement. It takes all of 
the relevant parameters in the request and persists this information in the database.
- **Delete Achievement** ​deletes a single achievement from the database by the
supplied achievement id.

## Starting a project

Spring tool suite 4 is my tool for Eclipse environment for this project. To create project do:

1. Choose create Spring starter project.
2. Configure: Management tool is Maven, Language is Java, Java version is 8 and packaging is jar.
3. Configure Spring boot version to 2.4.2.
4. Import dependencies: MySQL Driver, Spring Data JPA and Spring Web.
5. Configure *application.properties* in relation to your MySQL local host (username and password). Everything else should be same as in project's *application.properties*.
6. File *pom.xml* must be same as in this project.
7. Developing code.
8. Execute *createDBandTablesForGA.sql* file which can be found in project: *src -> main -> java/net/javaguides/springboot -> files*. This executed file will:
- create database *achievementsDB*;
- create tables *Games* and *Achievements*;
- insert three *Games* record in to the table *Games*;
9. Run *SpringbootCrudRestfullWebservicesGameAchievementsApiApplication.java* as Spring Boot App.
10. For testing can be used *Postman App*. That can be achieved by sending manually CRUD API requests. Also you can import Postman file *GameAchievements.postman_preparedRequests* (which is in the same directory as *createDBandTablesForGA.sql* file) and then send one by one prepared requests for all API operations.
11. Monitor the state of the base and Postman console while sending Postman requests.

[database.yml.mysql]: https://gitlab.com/gitlab-org/gitlab-foss/blob/master/config/database.yml.mysql
[svlogd]: http://smarden.org/runit/svlogd.8.html
[installation]: https://about.gitlab.com/install/
[gitlab.rb.template]: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template
