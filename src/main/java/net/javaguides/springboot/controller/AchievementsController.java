package net.javaguides.springboot.controller;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.entity.Achievements;
import net.javaguides.springboot.repository.AchievementsRepository;
import net.javaguides.springboot.repository.GamesRepository;
import net.javaguides.springboot.exception.BadRequestException;
import net.javaguides.springboot.exception.ResourceNotFoundException;

import java.sql.Timestamp;


@RestController
@RequestMapping("/api")
public class AchievementsController {

	@Autowired
	private AchievementsRepository achievementsRepository;
	@Autowired
	private GamesRepository gamesRepository;

	@GetMapping("/achievements")
	public List<Achievements> getAllAchievements(){
		
		return this.achievementsRepository.findAll();
		
	}
	
	@GetMapping("/achievements/{id}")
	public Achievements getAchievementById(@PathVariable (value = "id") String achievementsId) {
		
		return this.achievementsRepository.findById(achievementsId)
				.orElseThrow(() -> new ResourceNotFoundException("Achievement not found with id :" + achievementsId));
		
	}
	
	@GetMapping("/games/{gameId}/achievements") 
    public List<Achievements> getAllAchievementsByGameId(@PathVariable (value = "gameId") String gameId,
                                                Pageable pageable) {
		
		if(!(gamesRepository.findById(gameId).isPresent())) throw new
		
		BadRequestException("Impossible to get achievements for not existing game"
        		+ " with game id " + gameId);
		
		List<Achievements> resultList = achievementsRepository.findByGameId(gameId, pageable);
		
		Collections.sort(resultList);
	
        return resultList;
    }
	
	@PostMapping("/games/{gameId}/achievements")
    public Achievements createAchievement(@Valid @RequestBody Achievements achievements, @PathVariable (value = "gameId") String gameId) {

		if(!(gamesRepository.findById(gameId).isPresent())) throw new
		
		BadRequestException("Impossible to post new achievement for not existing game"
        		+ " with game id " + gameId);
		
		Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
		
		achievements.setCreatedTimestamp(currentTimestamp);
		
		
		Optional<Achievements> achievementsEntity = gamesRepository.findById(gameId).map(game -> {
			 achievements.setGame(game);
	            return achievementsRepository.save(achievements);
	        });
		
		return achievementsEntity.get();
    }

	@PutMapping("/achievements/{id}")
	public Achievements updateAchievement(@Valid @RequestBody Achievements achievements, @PathVariable ("id") String achievementsId) {
		
		Achievements existingAchievements = this.achievementsRepository.findById(achievementsId)
		.orElseThrow(() -> new ResourceNotFoundException("Achievement not found with id " + achievementsId));

		Timestamp timestampForUpdate = new Timestamp(System.currentTimeMillis());
		
		existingAchievements.setUpdatedTimestamp(timestampForUpdate);
		existingAchievements.setDisplayName(achievements.getDisplayName());
		existingAchievements.setDescription(achievements.getDescription());
		existingAchievements.setDisplayorder(achievements.getDisplayorder());
		existingAchievements.setIcon(achievements.getIcon());
		
		return this.achievementsRepository.save(existingAchievements);
	}
	
	@DeleteMapping("/achievements/{id}")
	public ResponseEntity<Achievements> deleteAchievement(@PathVariable ("id") String userId){
		
		Achievements existingAchievements = this.achievementsRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("Achievement not found with id " + userId));
		
		this.achievementsRepository.delete(existingAchievements);
		
		return ResponseEntity.ok().build();
	}
	
}
