package net.javaguides.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import net.javaguides.springboot.entity.Games;
import net.javaguides.springboot.repository.GamesRepository;


@RestController
@RequestMapping("/api/games")
public class GamesController {

	@Autowired
	private GamesRepository gamesRepository;
	
	@GetMapping
	public List<Games> getAllGames(){
		
		return this.gamesRepository.findAll();
		
	}
	
	@PostMapping
	public Games createGame(@RequestBody Games games) {
		
		return this.gamesRepository.save(games);
	}
	
}
