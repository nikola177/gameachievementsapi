package net.javaguides.springboot.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.validation.constraints.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity      
@Table(name = "achievements")  
public class Achievements implements Comparable<Achievements> {

		@Id
	    @GeneratedValue(generator = "UUID")
	    @GenericGenerator(
	        name = "UUID",
	    strategy = "org.hibernate.id.UUIDGenerator"
	    )
		@Column(updatable = false, nullable = false, unique = true)
		private String id;
			
		@NotNull(message="displayName must not be null.")
		@Size(min = 1, max = 100, message = "displayName must have at least 1 and maximum 100 characters.")
		@Column(name = "display_name", nullable = false)
		private String displayName;
		
		@NotNull(message="description must not be null.")
		@Size(min = 1, max = 500, message = "description must have at least 1 and maximum 500 characters.")
		@Column(name = "description")
		private String description;
		
		@OrderColumn
		@Column(name = "display_order")
		private int displayorder;

		@Column(name = "icon")
		private String icon;
		
		@Column(name = "created")
		private Timestamp createdTimestamp;
		
		@Column(name = "updated")
		private Timestamp updatedTimestamp;
		
		@ManyToOne(fetch = FetchType.EAGER, optional = false)
	    @JoinColumn(name = "game_id", nullable = false, updatable = false)
	    @OnDelete(action = OnDeleteAction.CASCADE)
	    private Games game;
		
		public Achievements() {
			
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getDisplayName() {
			return displayName;
		}

		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public int getDisplayorder() {
			return displayorder;
		}

		public void setDisplayorder(int displayorder) {
			this.displayorder = displayorder;
		}

		public String getIcon() {
			return icon;
		}

		public void setIcon(String icon) {
			this.icon = icon;
		}

		public Timestamp getCreatedTimestamp() {
			return createdTimestamp;
		}

		public void setCreatedTimestamp(Timestamp createdTimestamp) {
			this.createdTimestamp = createdTimestamp;
		}

		public Timestamp getUpdatedTimestamp() {
			return updatedTimestamp;
		}

		public void setUpdatedTimestamp(Timestamp updatedTimestamp) {
			this.updatedTimestamp = updatedTimestamp;
		}

		public Games getGame() {
			return game;
		}

		public void setGame(Games game) {
			this.game = game;
		}

		@Override
		public String toString() {
			return "Achievements [id=" + id + ", displayName=" + displayName + ", description=" + description
					+ ", displayorder=" + displayorder + ", icon=" + icon + ", createdTimestamp=" + createdTimestamp
					+ ", updatedTimestamp=" + updatedTimestamp + "]";
		}

		@Override
		public int compareTo(Achievements that) {
		
			if(this.getDisplayorder() == that.getDisplayorder()) return 0;
			
			else if(this.getDisplayorder() < that.getDisplayorder()) return -1;
			
			else return 1;
			
		}
		
}
