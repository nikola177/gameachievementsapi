create database achievementsDB;

CREATE TABLE `achievementsDB`.`games` (
  `id` varchar(50) NOT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE  `achievementsDB`.`achievements` (
  `id` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `description` varchar(500) NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `display_order` int DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `updated` datetime NULL,
  `game_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmm6cv053k2jjggcpcuv3qmpus` (`game_id`),
  CONSTRAINT `FKmm6cv053k2jjggcpcuv3qmpus` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `achievementsdb`.`games`
(`id`,
`display_name`)
VALUES
("wxa20143-hjdv-41ui-ih90-070yw7xdqw5m",
"Medal of honor");

INSERT INTO `achievementsdb`.`games`
(`id`,
`display_name`)
VALUES
("hxx105hp-qwq1-2g11-1ty7-abay55x5yxyvv",
"Warriors of a night");

INSERT INTO `achievementsdb`.`games`
(`id`,
`display_name`)
VALUES
("nigc1ghx-sw2a-2ikp-qbnm-2b5faghncmee7",
"Harry Potter");
