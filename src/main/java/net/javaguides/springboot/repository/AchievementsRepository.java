package net.javaguides.springboot.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Pageable;

import net.javaguides.springboot.entity.Achievements;

public interface AchievementsRepository extends JpaRepository<Achievements, String>{
	
	List<Achievements> findByGameId(String gamesId, Pageable pageable);
	
}
