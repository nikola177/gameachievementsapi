package net.javaguides.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.javaguides.springboot.entity.Games;

public interface GamesRepository extends JpaRepository<Games,String> {

}
